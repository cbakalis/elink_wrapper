# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir/.. $::origin_dir_loc
}

puts $origin_dir/../

# Set the project name
set project_name "elink_wrapper_tb"

# Use project name variable, if specified in the tcl shell
if { [info exists ::user_project_name] } {
  set project_name $::user_project_name
}

variable script_file
set script_file "build.tcl"

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir/.. <path>\]"
  puts "$script_file -tclargs \[--project_name <name>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir/.. <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir/.. path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--project_name <name>\] Create project with the specified name. Default"
  puts "                       name is the name of the project from where this"
  puts "                       script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir/.."   { incr i; set origin_dir/.. [lindex $::argv $i] }
      "--project_name" { incr i; set project_name [lindex $::argv $i] }
      "--help"         { help }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
}

# Create project
create_project $project_name $origin_dir/../$project_name -part xc7vx690tffg1761-2

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Reconstruct message rules
# None

# Set project properties
set obj [current_project]
set_property -name "board_connections" -value "" -objects $obj
set_property -name "board_part" -value "xilinx.com:vc709:part0:1.8" -objects $obj
set_property -name "compxlib.activehdl_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/activehdl" -objects $obj
set_property -name "compxlib.funcsim" -value "1" -objects $obj
set_property -name "compxlib.ies_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/ies" -objects $obj
set_property -name "compxlib.modelsim_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/modelsim" -objects $obj
set_property -name "compxlib.overwrite_libs" -value "0" -objects $obj
set_property -name "compxlib.questa_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/questa" -objects $obj
set_property -name "compxlib.riviera_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/riviera" -objects $obj
set_property -name "compxlib.timesim" -value "1" -objects $obj
set_property -name "compxlib.vcs_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/vcs" -objects $obj
set_property -name "compxlib.xsim_compiled_library_dir" -value "" -objects $obj
set_property -name "corecontainer.enable" -value "1" -objects $obj
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "dsa.num_compute_units" -value "16" -objects $obj
set_property -name "dsa.rom.debug_type" -value "0" -objects $obj
set_property -name "dsa.rom.prom_type" -value "0" -objects $obj
set_property -name "enable_optional_runs_sta" -value "0" -objects $obj
set_property -name "generate_ip_upgrade_log" -value "1" -objects $obj
set_property -name "ip_cache_permissions" -value "read write" -objects $obj
set_property -name "ip_interface_inference_priority" -value "" -objects $obj
set_property -name "ip_output_repo" -value "$proj_dir/${project_name}.cache/ip" -objects $obj
set_property -name "managed_ip" -value "0" -objects $obj
set_property -name "pr_flow" -value "0" -objects $obj
set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
set_property -name "sim.use_ip_compiled_libs" -value "1" -objects $obj
set_property -name "simulator_language" -value "Mixed" -objects $obj
set_property -name "source_mgmt_mode" -value "All" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj
set_property -name "target_simulator" -value "XSim" -objects $obj
set_property -name "xpm_libraries" -value "XPM_CDC XPM_MEMORY" -objects $obj
set_property -name "xsim.array_display_limit" -value "1024" -objects $obj
set_property -name "xsim.radix" -value "hex" -objects $obj
set_property -name "xsim.time_unit" -value "ns" -objects $obj
set_property -name "xsim.trace_limit" -value "65536" -objects $obj

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# top
read_vhdl -vhdl2008 -library work $origin_dir/../sources/sim/elink_wrapper_top.vhd

# elinks
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/Elink2FIFO.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/FIFO2Elink.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_HDLC.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_direct.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN8_DEC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2_HDLC.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/upstreamEpathFifoWrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT4_direct.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN4_DEC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN2_DEC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT2_ENC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT4_ENC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT8_ENC8b10b.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/elinkRXfifo_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/pulse_fall_pw01.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX8_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/enc_8b10.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX2_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/MUX4_Nbit.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/pulse_pdxx_pwxx.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/enc8b10_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT4.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_OUT8.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/8b10_dec.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/8b10_dec_wrap.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN4.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/EPROC_IN8.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/KcharTest.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/centralRouter_package.vhd
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/elinks/elink_wrapper.vhd

# misc
read_vhdl -vhdl2008 -library work $origin_dir/../sources/src/misc/CDCC.vhd


# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
set files [list \
 "[file normalize "$origin_dir/../sources/sim/elink_wrapper_top_tb.vhd"]"\
]
add_files -norecurse -fileset $obj $files

# Set 'sim_1' fileset file properties for remote files
set file "$origin_dir/../sources/sim/elink_wrapper_top_tb.vhd"
set file [file normalize $file]
set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
set_property "file_type" "VHDL" $file_obj

# Set 'sim_1' fileset file properties for remote files
# set file "$origin_dir/../sources/sim/scax_package_simul.vhd"
# set file [file normalize $file]
# set file_obj [get_files -of_objects [get_filesets sim_1] [list "*$file"]]
# set_property "file_type" "VHDL" $file_obj

# IP cores
set obj [get_filesets sources_1]   
set files [list \
 "[file normalize "$origin_dir/../sources/sim/mmcm_master.xcix"]"\
 "[file normalize "$origin_dir/../sources/ip/fh_epath_fifo2K_18bit_wide_bif_v7.xcix"]"\
 "[file normalize "$origin_dir/../sources/ip/fh_epath_fifo2K_18bit_wide_bif.xcix"]"\
 "[file normalize "$origin_dir/../sources/ip/hdlc_bist_fifo.xcix"]"\
 "[file normalize "$origin_dir/../sources/ip/upstreamFIFO.xcix"]"\
]

add_files -norecurse -fileset $obj $files

set_property target_language VHDL [current_project]

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property "top" "elink_wrapper_top" $obj

puts "###################################"
puts "INFO: Project created:$project_name"
puts "###################################"
puts "Build Succesful. Enjoy :)"

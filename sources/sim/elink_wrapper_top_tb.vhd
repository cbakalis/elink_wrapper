
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity elink_wrapper_top_tb is
end;

architecture bench of elink_wrapper_top_tb is

  component elink_wrapper_top
    generic(
      TX_dataRate         : integer := 320;
      RX_dataRate         : integer := 320;
      TXRX_elinkEncoding  : std_logic_vector (1 downto 0) := "01";
      serialized_input    : boolean := false
    ); 
    port(
      clk           : in  std_logic;
      rst           : in  std_logic;
      drdy_dbg      : out std_logic; 
      din_dbg       : out std_logic_vector(9 downto 0); 
      rx_elink_dbg  : out std_logic;
      tx_elink_dbg  : out std_logic;
      rx_fifo_dout  : out std_logic_vector(9 downto 0);
      rx_fifo_full  : out std_logic;
      tx_fifo_wr    : in  std_logic;
      tx_fifo_din   : in  std_logic_vector(17 downto 0);
      tx_fifo_pfull : out std_logic
    );
  end component;

  signal clk: std_logic := '0';
  signal rst: std_logic := '0';
  signal drdy_dbg: std_logic := '0';
  signal din_dbg: std_logic_vector(9 downto 0) := (others => '0');
  signal rx_elink_dbg: std_logic := '0';
  signal tx_elink_dbg: std_logic := '0';
  signal rx_fifo_dout: std_logic_vector(9 downto 0) := (others => '0');
  signal rx_fifo_full: std_logic := '0';
  signal tx_fifo_wr: std_logic := '0';
  signal tx_fifo_din: std_logic_vector(17 downto 0) := (others => '0');
  signal tx_fifo_pfull: std_logic := '0';

  constant clock_period: time := 25 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: elink_wrapper_top generic map ( TX_dataRate        => 80,
                                       RX_dataRate        => 80,
                                       TXRX_elinkEncoding => "01",
                                       serialized_input   =>  false)
                            port map ( clk                => clk,
                                       rst                => rst,
                                       drdy_dbg           => drdy_dbg,
                                       din_dbg            => din_dbg,
                                       rx_elink_dbg       => rx_elink_dbg,
                                       tx_elink_dbg       => tx_elink_dbg,
                                       rx_fifo_dout       => rx_fifo_dout,
                                       rx_fifo_full       => rx_fifo_full,
                                       tx_fifo_wr         => tx_fifo_wr,
                                       tx_fifo_din        => tx_fifo_din,
                                       tx_fifo_pfull      => tx_fifo_pfull );

  stimulus: process
  begin
  
  wait for clock_period*500;
  tx_fifo_wr <= '1';
  tx_fifo_din <= "10" & x"0000";

  wait for clock_period;
  tx_fifo_din <= "00" & x"0123";

  wait for clock_period;
  tx_fifo_din <= "00" & x"4567";

  wait for clock_period;
  tx_fifo_din <= "00" & x"89ab";

  wait for clock_period;
  tx_fifo_din <= "00" & x"cdef";

  wait for clock_period;
  tx_fifo_din <= "01" & x"0000";

  wait for clock_period;
  tx_fifo_wr <= '0';




    -- Put test bench stimulus code here

    stop_the_clock <= false;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;
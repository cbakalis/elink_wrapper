-- for testing

library UNISIM; 
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 
use UNISIM.VComponents.all; 

entity elink_wrapper_top is
  generic(
    TX_dataRate         : integer := 320;   -- 8b10b: 80, 160, 320 are supported. HDLC: only 80
    RX_dataRate         : integer := 320;   -- 8b10b: 80, 160, 320 are supported. HDLC: only 80
    TXRX_elinkEncoding  : std_logic_vector (1 downto 0) := "01"; -- only "01" (8b10b) or "10" (HDLC)
    serialized_input    : boolean := false -- serial or parallel input
  ); 
  port(
    -----------------------------
    ------ General Interface ----
    clk           : in  std_logic;
    rst           : in  std_logic;
    drdy_dbg      : out std_logic; 
    din_dbg       : out std_logic_vector(9 downto 0); 
    rx_elink_dbg  : out std_logic;
    tx_elink_dbg  : out std_logic;
    -----------------------------
    ------ RX Interface ---------
    rx_fifo_dout  : out std_logic_vector(9 downto 0);  -- rx fifo decoded data
    rx_fifo_full  : out std_logic;
    -----------------------------
    ------ TX Interface ---------
    tx_fifo_wr    : in  std_logic; -- tx fifo wr_ena
    tx_fifo_din   : in  std_logic_vector(17 downto 0); -- tx fifo din
    tx_fifo_pfull : out std_logic -- tx fifo prog full
  );
end elink_wrapper_top; 

architecture Behavioral of elink_wrapper_top is
  
component mmcm_master
  port(
    -- Clock in ports
    clk_in1 : in std_logic; 
    -- Clock out ports
    clk_out_40  : out std_logic; 
    clk_out_80  : out std_logic; 
    clk_out_160 : out std_logic; 
    clk_out_320 : out std_logic; 
    -- Status and control signals
    reset  : in  std_logic; 
    locked : out std_logic
  ); 
end component; 

component elink_wrapper
  generic(
    TX_dataRate         : integer := 320;   -- 8b10b: 80, 160, 320 are supported. HDLC: only 80
    RX_dataRate         : integer := 320;   -- 8b10b: 80, 160, 320 are supported. HDLC: only 80
    TXRX_elinkEncoding  : std_logic_vector (1 downto 0) := "01"; -- only "01" (8b10b) or "10" (HDLC)
    serialized_input    : boolean := false -- serial or parallel input
  ); 
  port(
    -----------------------------
    ------ General Interface ----
    clk_usr       : in  std_logic; -- user clock (rd/wr fifo domain)
    clk_40        : in  std_logic; -- elink clock
    clk_80        : in  std_logic; -- elink clock
    clk_160       : in  std_logic; -- elink clock
    clk_320       : in  std_logic; -- elink clock
    rst           : in  std_logic; -- reset the core. must be high at init
    reverse_rx    : in  std_logic; 
    reverse_tx    : in  std_logic; 
    drdy_dbg      : out std_logic; 
    din_dbg       : out std_logic_vector(9 downto 0); 
    rx_elink_dbg  : out std_logic; -- for debugging (ILA)
    tx_elink_dbg  : out std_logic; -- for debugging (ILA)
    -----------------------------
    ------ RX Interface ---------
    rx_elink      : in  std_logic; -- 1-bit elink rx
    rx_elink2bit  : in  std_logic_vector(1 downto 0);  -- 2-bit elink rx
    rx_elink4bit  : in  std_logic_vector(3 downto 0);  -- 4-bit elink rx
    rx_elink8bit  : in  std_logic_vector(7 downto 0);  -- 8-bit elink rx
    rx_swap       : in  std_logic; -- swap input bits
    rx_fifo_rd    : in  std_logic; -- rx fifo rd_ena
    rx_fifo_empty : out std_logic; -- rx fifo empty
    rx_fifo_full  : out std_logic; -- rx fifo full
    rx_fifo_dout  : out std_logic_vector(9 downto 0);  -- rx fifo decoded data
    -----------------------------
    ------ TX Interface ---------
    tx_swap       : in  std_logic; -- swap output bits
    tx_fifo_wr    : in  std_logic; -- tx fifo wr_ena
    tx_fifo_din   : in  std_logic_vector(17 downto 0); -- tx fifo din
    tx_fifo_pfull : out std_logic; -- tx fifo prog full
    tx_elink      : out std_logic; -- 1-bit elink tx
    tx_elink2bit  : out std_logic_vector(1 downto 0); -- 2-bit elink tx
    tx_elink4bit  : out std_logic_vector(3 downto 0); -- 4-bit elink tx
    tx_elink8bit  : out std_logic_vector(7 downto 0)  -- 8-bit elink tx
  ); 
end component; 

  signal clk_40         : std_logic := '0'; 
  signal clk_80         : std_logic := '0'; 
  signal clk_160        : std_logic := '0'; 
  signal clk_320        : std_logic := '0'; 
  
  signal rst_i          : std_logic := '0'; 
  signal mmcm_locked    : std_logic := '0'; 

  signal rx_fifo_rd     : std_logic := '0';
  signal rx_fifo_empty  : std_logic := '0';

  signal tx_fifo_wr_i   : std_logic := '0';
  signal tx_fifo_din_i  : std_logic_vector(17 downto 0) := (others => '0');

  signal elink_1bit_i   : std_logic := '0'; 
  signal elink_2bit_i   : std_logic_vector(1 downto 0) := (others => '0'); 
  signal elink_4bit_i   : std_logic_vector(3 downto 0) := (others => '0'); 
  signal elink_8bit_i   : std_logic_vector(7 downto 0) := (others => '0'); 
  
begin
  
mmcm_master_inst : mmcm_master
  port map( 
    -- Clock out ports  
    clk_out_40  => clk_40,
    clk_out_80  => clk_80,
    clk_out_160 => clk_160,
    clk_out_320 => clk_320,
    -- Status and control signals                
    reset       => rst,
    locked      => mmcm_locked,
    -- Clock in ports
    clk_in1     => clk
  ); 

elink_wrapper_inst : elink_wrapper
  generic map(
    TX_dataRate         => TX_dataRate,
    RX_dataRate         => RX_dataRate,
    TXRX_elinkEncoding  => TXRX_elinkEncoding,
    serialized_input    => serialized_input
  )
  port map(
    -----------------------------
    ------ General Interface ----
    clk_usr       => clk,
    clk_40        => clk_40,
    clk_80        => clk_80,
    clk_160       => clk_160,
    clk_320       => clk_320,
    rst           => rst_i,
    reverse_rx    => '0',
    reverse_tx    => '0',
    drdy_dbg      => drdy_dbg,
    din_dbg       => din_dbg,
    rx_elink_dbg  => rx_elink_dbg,
    tx_elink_dbg  => tx_elink_dbg,
    -----------------------------
    ------ RX Interface ---------
    rx_elink      => elink_1bit_i,
    rx_elink2bit  => elink_2bit_i,
    rx_elink4bit  => elink_4bit_i,
    rx_elink8bit  => elink_8bit_i,
    rx_swap       => '0',
    rx_fifo_rd    => rx_fifo_rd,
    rx_fifo_empty => rx_fifo_empty,
    rx_fifo_full  => rx_fifo_full,
    rx_fifo_dout  => rx_fifo_dout,
    -----------------------------
    ------ TX Interface ---------
    tx_swap       => '0',
    tx_fifo_wr    => tx_fifo_wr_i,
    tx_fifo_din   => tx_fifo_din_i,
    tx_fifo_pfull => tx_fifo_pfull,
    tx_elink      => elink_1bit_i,
    tx_elink2bit  => elink_2bit_i,
    tx_elink4bit  => elink_4bit_i,
    tx_elink8bit  => elink_8bit_i
  ); 

reg_wr_proc: process(clk)
begin
  if(rising_edge(clk))then
    tx_fifo_wr_i  <= tx_fifo_wr;
    tx_fifo_din_i <= tx_fifo_din;
  end if;
end process;

rd_fifo_proc: process(clk)
begin
  if(rising_edge(clk))then
    if(mmcm_locked = '1' and rx_fifo_empty = '0' and rx_fifo_rd = '0')then
      rx_fifo_rd <= '1';
    else
      rx_fifo_rd <= '0';
    end if;
  end if;
end process;

rst_i <= rst or (not mmcm_locked);
  
end Behavioral; 

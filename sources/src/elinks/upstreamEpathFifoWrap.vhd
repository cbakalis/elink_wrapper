----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    23/06/2015 
--! Module Name:    upstreamEpathFifoWrap
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee,work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;

--! EPATH FIFO 18 bit wide, 1K deep
entity upstreamEpathFifoWrap is
generic     (
                GBTid                           : integer := 0;
                egroupID                        : integer := 0;
                epathID                         : integer := 0;
                EnableFrHo_Egroup0Eproc2_HDLC   : boolean := true;
                EnableFrHo_Egroup0Eproc2_8b10b  : boolean := true;
                EnableFrHo_Egroup0Eproc4_8b10b  : boolean := true;
                EnableFrHo_Egroup0Eproc8_8b10b  : boolean := true;
                EnableFrHo_Egroup1Eproc2_HDLC   : boolean := true;
                EnableFrHo_Egroup1Eproc2_8b10b  : boolean := true;
                EnableFrHo_Egroup1Eproc4_8b10b  : boolean := true;
                EnableFrHo_Egroup1Eproc8_8b10b  : boolean := true;
                EnableFrHo_Egroup2Eproc2_HDLC   : boolean := true;
                EnableFrHo_Egroup2Eproc2_8b10b  : boolean := true;
                EnableFrHo_Egroup2Eproc4_8b10b  : boolean := true;
                EnableFrHo_Egroup2Eproc8_8b10b  : boolean := true;
                EnableFrHo_Egroup3Eproc2_HDLC   : boolean := true;
                EnableFrHo_Egroup3Eproc2_8b10b  : boolean := true;
                EnableFrHo_Egroup3Eproc4_8b10b  : boolean := true;
                EnableFrHo_Egroup3Eproc8_8b10b  : boolean := true;
                EnableFrHo_Egroup4Eproc2_HDLC   : boolean := true;
                EnableFrHo_Egroup4Eproc2_8b10b  : boolean := true;
                EnableFrHo_Egroup4Eproc4_8b10b  : boolean := true;
                EnableFrHo_Egroup4Eproc8_8b10b  : boolean := true;
                EnableFrHo_Egroup5Eproc2_HDLC   : boolean := true;
                EnableFrHo_Egroup5Eproc2_8b10b  : boolean := true;
                includeNoEncodingCase           : boolean := true;
				GENERATE_FEI4B  				: boolean := false;
				CARD_TYPE                       : integer
            );
port (   
    rst             : in std_logic;
    fifoFLUSH       : in std_logic;
    ---
    wr_clk          : in std_logic;
    wr_en           : in std_logic;
    din             : in std_logic_vector(17 downto 0);    
    ---
    rd_clk          : in std_logic;
    rd_en           : in std_logic;
    dout            : out std_logic_vector(9 downto 0);
    doutRdy         : out std_logic;
    ---
    full            : out std_logic;
    empty           : out std_logic;
    prog_full       : out std_logic
    );
end upstreamEpathFifoWrap;

architecture Behavioral of upstreamEpathFifoWrap is

----------------------------------
----------------------------------
COMPONENT fh_epath_fifo2K_18bit_wide_bif
  PORT (
    srst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    prog_full : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT fh_epath_fifo2K_18bit_wide_bif_v7
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    prog_full : OUT STD_LOGIC
  );
END COMPONENT;
----------------------------------
----------------------------------

signal rd_en_s, empty_efifo, empty_efifo_r, prog_full_s : std_logic;
signal OE, rst_state, byte_cnt,byte_mux_sel,byte_rdy,rd_en1,rd_en2 : std_logic := '0';
signal dout18bit : std_logic_vector(17 downto 0);
signal byte0, byte1 : std_logic_vector(9 downto 0) := "1100000000";
constant comma_byte : std_logic_vector(9 downto 0) := "1100000000";

signal byte0_code,byte1_code,word16_code : std_logic_vector(1 downto 0);
signal empty_efifo0, empty_efifo1,empty_efifo2 : std_logic := '1';

signal wr_en_r  : std_logic := '0';
signal din_r    : std_logic_vector(17 downto 0) := "110000000000000000";
signal rd_en_r  : std_logic;

begin

-- enable the EPATH_FIFO_WRAP when it is in use
Module_enable: if (
                        -- egroup 0
                        ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup0Eproc4_8b10b  = true) and (egroupID = 0) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup0Eproc8_8b10b  = true) and (egroupID = 0) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup0Eproc4_8b10b  = true) and (egroupID = 0) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup0Eproc4_8b10b  = true) and (egroupID = 0) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup0Eproc8_8b10b  = true) and (egroupID = 0) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup0Eproc4_8b10b  = true) and (egroupID = 0) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 7) ) or
                        ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 7) ) or
                        -- egroup 1
                        ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup1Eproc4_8b10b  = true) and (egroupID = 1) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup1Eproc8_8b10b  = true) and (egroupID = 1) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup1Eproc4_8b10b  = true) and (egroupID = 1) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup1Eproc4_8b10b  = true) and (egroupID = 1) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup1Eproc8_8b10b  = true) and (egroupID = 1) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup1Eproc4_8b10b  = true) and (egroupID = 1) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 7) ) or
                        ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 7) ) or
                        -- egroup 2
                        ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup2Eproc4_8b10b  = true) and (egroupID = 2) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup2Eproc8_8b10b  = true) and (egroupID = 2) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup2Eproc4_8b10b  = true) and (egroupID = 2) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup2Eproc4_8b10b  = true) and (egroupID = 2) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup2Eproc8_8b10b  = true) and (egroupID = 2) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup2Eproc4_8b10b  = true) and (egroupID = 2) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 7) ) or
                        ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 7) ) or
                        -- egroup 3
                        ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup3Eproc4_8b10b  = true) and (egroupID = 3) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup3Eproc8_8b10b  = true) and (egroupID = 3) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup3Eproc4_8b10b  = true) and (egroupID = 3) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup3Eproc4_8b10b  = true) and (egroupID = 3) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup3Eproc8_8b10b  = true) and (egroupID = 3) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup3Eproc4_8b10b  = true) and (egroupID = 3) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 7) ) or
                        ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 7) ) or
                        -- egroup 4
                        ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup4Eproc4_8b10b  = true) and (egroupID = 4) and (epathID = 0) ) or
                        ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup4Eproc8_8b10b  = true) and (egroupID = 4) and (epathID = 1) ) or
                        ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup4Eproc4_8b10b  = true) and (egroupID = 4) and (epathID = 2) ) or
                        ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 3) ) or
                        ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup4Eproc4_8b10b  = true) and (egroupID = 4) and (epathID = 4) ) or
                        ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup4Eproc8_8b10b  = true) and (egroupID = 4) and (epathID = 5) ) or
                        ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup4Eproc4_8b10b  = true) and (egroupID = 4) and (epathID = 6) ) or
                        ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 7) ) or
                        ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 7) ) or
                        -- egroup 5 (EC E-link)
                        ( (EnableFrHo_Egroup5Eproc2_HDLC   = true) and (egroupID = 5)                   ) or
                        ( (EnableFrHo_Egroup5Eproc2_8b10b  = true) and (egroupID = 5)                   ) or
                        ( (includeNoEncodingCase           = true)                                      )
                    ) generate


-------------------------------------------------------------------------------------------
-- write pipeline
-------------------------------------------------------------------------------------------
process(wr_clk)
begin
	if wr_clk'event and wr_clk = '1' then       
        wr_en_r   <= wr_en;
        din_r     <= din;
	end if;
end process;
--
-------------------------------------------------------------------------------------------
-- FIFO - ip
-------------------------------------------------------------------------------------------
g_ku: if (CARD_TYPE = 711 or CARD_TYPE = 712) generate
epathFIFO: fh_epath_fifo2K_18bit_wide_bif
port map(
    wr_clk      => wr_clk, 
    srst      => fifoFLUSH,
    rd_clk      => rd_clk, 
    din         => din_r,
    wr_en       => wr_en_r,
    rd_en       => rd_en_s,
    dout        => dout18bit, --18 bit
    full        => full,
    empty       => empty_efifo, 
    prog_full   => prog_full_s, -- 1008/960 from 1024
    wr_rst_busy => open,
    rd_rst_busy => open
    );  
end generate;
g_v7: if (CARD_TYPE = 709 or CARD_TYPE = 710) generate
epathFIFO: fh_epath_fifo2K_18bit_wide_bif_v7
port map(
    wr_clk      => wr_clk, 
    rst         => fifoFLUSH,
    rd_clk      => rd_clk, 
    din         => din_r,
    wr_en       => wr_en_r,
    rd_en       => rd_en_s,
    dout        => dout18bit, --18 bit
    full        => full,
    empty       => empty_efifo, 
    prog_full   => prog_full_s -- 1008/960 from 1024
    );  
end generate;

--Trick to keep the readout logic think that the fifo was empty 
empty_efifo0 <= '1' when (rd_en_r = '1' and empty_efifo_r = '1')
                    else empty_efifo;
--
-------------------------------------------------------------------------------------------
-- re pulse
-------------------------------------------------------------------------------------------
process(rd_clk,rst)
begin
	if rst = '1' then
        byte_cnt <= '0';
	elsif rd_clk'event and rd_clk = '1' then       
--IG        if rd_en = '1' then -- 1 clk trigger 
        if ((rd_en = '1') and ((empty_efifo1 = '0') or (empty_efifo1 = '1' and empty_efifo0 = '0'))) then -- 1 clk trigger  --IG: toggle the byte_cnt only when the FIFO is not empty 
                byte_cnt <= not byte_cnt;
        -- IG: add default value
        else
            byte_cnt <= byte_cnt;
        end if;
	end if;
end process;
--
rd_en_s <= rd_en and (not byte_cnt) and (not empty_efifo0); -- only when byte_cnt = 0
--
word16_code <= dout18bit(17 downto 16);
--
process(word16_code,empty_efifo1,empty_efifo2)
begin
    if word16_code = "10" then -- start of message
        byte0_code <= "11";
        byte1_code <= "1"& (empty_efifo2 and empty_efifo1);
    elsif word16_code = "01" then -- end of message
        byte0_code <= (empty_efifo1 and empty_efifo0)&"1";
        byte1_code <= "11";
    else -- "00" data
        byte0_code <= (empty_efifo1 and empty_efifo0) & (empty_efifo1 and empty_efifo0);
        byte1_code <= (empty_efifo2 and empty_efifo1) & (empty_efifo2 and empty_efifo1);
    end if;
end process;
--
GBT_mode: if (GENERATE_FEI4B = false) generate
	byte0 <= byte0_code & dout18bit(15 downto 8);
	byte1 <= byte1_code & dout18bit(7 downto 0);
	--byte0 <= byte0_code & dout18bit(7 downto 0);
	--byte1 <= byte1_code & dout18bit(15 downto 8);
end generate GBT_mode;

FEI4B: if (GENERATE_FEI4B = true) generate
	byte0 <= byte0_code & dout18bit(8) & dout18bit(9) & dout18bit(10) & dout18bit(11) & dout18bit(12) & dout18bit(13) & dout18bit(14) & dout18bit(15);
	byte1 <= byte1_code & dout18bit(0) & dout18bit(1) & dout18bit( 2) & dout18bit( 3) & dout18bit( 4) & dout18bit( 5) & dout18bit( 6) & dout18bit( 7);
end generate FEI4B;

--
process(byte_cnt,byte0,byte1)
begin
	if byte_cnt = '1' then  
            dout  <= byte0;
	else 
            dout  <= byte1;
	end if;
end process;
--
--
process(rd_clk)
begin
	if rd_clk'event and rd_clk = '1' then  
        byte_rdy <= byte_cnt;
	end if;
end process;
--

-------------------------------------------------------------------------------------------
-- re pulse
-------------------------------------------------------------------------------------------
--
process(rd_clk)
begin
	if rd_clk'event and rd_clk = '1' then  
        doutRdy <= rd_en;   
        empty_efifo_r <= empty_efifo0;
        rd_en_r <= rd_en;
        if rd_en = '1' then
            empty_efifo1 <= empty_efifo0;
            empty_efifo2 <= empty_efifo1;  
        end if; 
	end if;
end process;
--
empty <= empty_efifo0;-- rd_clk domain
--
process(rd_clk)
begin
	if rd_clk'event and rd_clk = '1' then
        rst_state   <= rst or fifoFLUSH; 
        OE          <= not rst_state;      
	end if;
end process;
--
prog_full   <= prog_full_s and OE;
--

end generate Module_enable;


Module_disable: if  ( 
                not(
                        -- egroup 0
                    ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup0Eproc4_8b10b  = true) and (egroupID = 0) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup0Eproc8_8b10b  = true) and (egroupID = 0) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup0Eproc4_8b10b  = true) and (egroupID = 0) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup0Eproc4_8b10b  = true) and (egroupID = 0) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup0Eproc8_8b10b  = true) and (egroupID = 0) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup0Eproc4_8b10b  = true) and (egroupID = 0) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup0Eproc2_HDLC   = true) and (egroupID = 0) and (epathID = 7) ) or
                    ( (EnableFrHo_Egroup0Eproc2_8b10b  = true) and (egroupID = 0) and (epathID = 7) ) or
                    -- egroup 1
                    ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup1Eproc4_8b10b  = true) and (egroupID = 1) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup1Eproc8_8b10b  = true) and (egroupID = 1) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup1Eproc4_8b10b  = true) and (egroupID = 1) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup1Eproc4_8b10b  = true) and (egroupID = 1) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup1Eproc8_8b10b  = true) and (egroupID = 1) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup1Eproc4_8b10b  = true) and (egroupID = 1) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup1Eproc2_HDLC   = true) and (egroupID = 1) and (epathID = 7) ) or
                    ( (EnableFrHo_Egroup1Eproc2_8b10b  = true) and (egroupID = 1) and (epathID = 7) ) or
                    -- egroup 2
                    ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup2Eproc4_8b10b  = true) and (egroupID = 2) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup2Eproc8_8b10b  = true) and (egroupID = 2) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup2Eproc4_8b10b  = true) and (egroupID = 2) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup2Eproc4_8b10b  = true) and (egroupID = 2) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup2Eproc8_8b10b  = true) and (egroupID = 2) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup2Eproc4_8b10b  = true) and (egroupID = 2) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup2Eproc2_HDLC   = true) and (egroupID = 2) and (epathID = 7) ) or
                    ( (EnableFrHo_Egroup2Eproc2_8b10b  = true) and (egroupID = 2) and (epathID = 7) ) or
                    -- egroup 3
                    ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup3Eproc4_8b10b  = true) and (egroupID = 3) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup3Eproc8_8b10b  = true) and (egroupID = 3) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup3Eproc4_8b10b  = true) and (egroupID = 3) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup3Eproc4_8b10b  = true) and (egroupID = 3) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup3Eproc8_8b10b  = true) and (egroupID = 3) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup3Eproc4_8b10b  = true) and (egroupID = 3) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup3Eproc2_HDLC   = true) and (egroupID = 3) and (epathID = 7) ) or
                    ( (EnableFrHo_Egroup3Eproc2_8b10b  = true) and (egroupID = 3) and (epathID = 7) ) or
                    -- egroup 4
                    ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup4Eproc4_8b10b  = true) and (egroupID = 4) and (epathID = 0) ) or
                    ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup4Eproc8_8b10b  = true) and (egroupID = 4) and (epathID = 1) ) or
                    ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup4Eproc4_8b10b  = true) and (egroupID = 4) and (epathID = 2) ) or
                    ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 3) ) or
                    ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup4Eproc4_8b10b  = true) and (egroupID = 4) and (epathID = 4) ) or
                    ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup4Eproc8_8b10b  = true) and (egroupID = 4) and (epathID = 5) ) or
                    ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup4Eproc4_8b10b  = true) and (egroupID = 4) and (epathID = 6) ) or
                    ( (EnableFrHo_Egroup4Eproc2_HDLC   = true) and (egroupID = 4) and (epathID = 7) ) or
                    ( (EnableFrHo_Egroup4Eproc2_8b10b  = true) and (egroupID = 4) and (epathID = 7) ) or
                    -- egroup 5 (EC E-link)
                    ( (EnableFrHo_Egroup5Eproc2_HDLC   = true) and (egroupID = 5)                   ) or
                    ( (EnableFrHo_Egroup5Eproc2_8b10b  = true) and (egroupID = 5)                   ) or
                    ( (includeNoEncodingCase           = true)                                      )
                    )
                    ) generate


    dout            <= (others => '0');
    doutRdy         <= '0';
    ---
    full            <= '0';
    empty           <= '1';
    prog_full       <= '0';

end generate Module_disable;



end Behavioral;


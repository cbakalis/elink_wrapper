----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of the SCA_eXtension_firmware (SCAX).
--
--    SCAX is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    SCAX is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with SCAX.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 28.08.2018 00:12:10
-- Design Name: Elink Wrapper
-- Module Name: elink_wrapper - RTL
-- Project Name: SCAX
-- Target Devices: Xilinx 7-Series
-- Tool Versions: Vivado 2018.2
-- Description: Wrapper for the E-Link modules
--
-- Dependencies: FELIX E-link/central router modules
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 
use IEEE.NUMERIC_STD.ALL; 

entity elink_wrapper is
  generic(
    TX_dataRate         : integer := 320;   -- 8b10b: 80, 160, 320 are supported. HDLC: only 80
    RX_dataRate         : integer := 320;   -- 8b10b: 80, 160, 320 are supported. HDLC: only 80
    TXRX_elinkEncoding  : std_logic_vector (1 downto 0) := "01"; -- only "01" (8b10b) or "10" (HDLC)
    serialized_input    : boolean := false -- serial or parallel input
  ); 
  port(
    -----------------------------
    ------ General Interface ----
    clk_usr       : in  std_logic; -- user clock (rd/wr fifo domain)
    clk_40        : in  std_logic; -- elink clock
    clk_80        : in  std_logic; -- elink clock
    clk_160       : in  std_logic; -- elink clock
    clk_320       : in  std_logic; -- elink clock
    rst           : in  std_logic; -- reset the core. must be high at init
    reverse_rx    : in  std_logic; 
    reverse_tx    : in  std_logic; 
    drdy_dbg      : out std_logic; 
    din_dbg       : out std_logic_vector(9 downto 0); 
    rx_elink_dbg  : out std_logic; -- for debugging (ILA)
    tx_elink_dbg  : out std_logic; -- for debugging (ILA)
    -----------------------------
    ------ RX Interface ---------
    rx_elink      : in  std_logic; -- 1-bit elink rx
    rx_elink2bit  : in  std_logic_vector(1 downto 0);  -- 2-bit elink rx
    rx_elink4bit  : in  std_logic_vector(3 downto 0);  -- 4-bit elink rx
    rx_elink8bit  : in  std_logic_vector(7 downto 0);  -- 8-bit elink rx
    rx_swap       : in  std_logic; -- swap input bits
    rx_fifo_rd    : in  std_logic; -- rx fifo rd_ena
    rx_fifo_empty : out std_logic; -- rx fifo empty
    rx_fifo_full  : out std_logic; -- rx fifo full
    rx_fifo_dout  : out std_logic_vector(9 downto 0);  -- rx fifo decoded data
    -----------------------------
    ------ TX Interface ---------
    tx_swap       : in  std_logic; -- swap output bits
    tx_fifo_wr    : in  std_logic; -- tx fifo wr_ena
    tx_fifo_din   : in  std_logic_vector(17 downto 0); -- tx fifo din
    tx_fifo_pfull : out std_logic; -- tx fifo prog full
    tx_elink      : out std_logic; -- 1-bit elink tx
    tx_elink2bit  : out std_logic_vector(1 downto 0); -- 2-bit elink tx
    tx_elink4bit  : out std_logic_vector(3 downto 0); -- 4-bit elink tx
    tx_elink8bit  : out std_logic_vector(7 downto 0)  -- 8-bit elink tx
  ); 
end elink_wrapper; 

architecture RTL of elink_wrapper is
  
component FIFO2Elink
  generic (
    OutputDataRate : integer := 320;  -- 80 / 160 / 320 MHz
    elinkEncoding  : std_logic_vector (1 downto 0) := "01" -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
  ); 
  port ( 
    clk40       : in std_logic; 
    clk80       : in std_logic; 
    clk160      : in std_logic; 
    clk320      : in std_logic; 
    rst         : in std_logic; 
    fifo_flush  : in std_logic; 
    swap_output : in std_logic; 
    reverse_tx  : in std_logic; 
    ------
    efifoWclk   : in  std_logic; 
    efifoDin    : in  std_logic_vector (17 downto 0); -- [data_code,2bit][data,16bit]
    efifoWe     : in  std_logic; 
    efifoPfull  : out std_logic; 
    ------
    DATA1bitOUT : out std_logic; -- serialized output
    elink2bit   : out std_logic_vector (1 downto 0); -- 2 bits @ clk40, can interface 2-bit of GBT frame
    elink4bit   : out std_logic_vector (3 downto 0); -- 4 bits @ clk40, can interface 4-bit of GBT frame
    elink8bit   : out std_logic_vector (7 downto 0)  -- 8 bits @ clk40, can interface 8-bit of GBT frame
    ------
  ); 
end component; 
  
component Elink2FIFO
  generic (
    InputDataRate    : integer := 320;   -- 80 / 160 / 320 / 640 MHz
    elinkEncoding    : std_logic_vector (1 downto 0) := "01"; -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
    serialized_input : boolean := true
  ); 
  port ( 
    clk40      : in  std_logic; 
    clk80      : in  std_logic; 
    clk160     : in  std_logic; 
    clk320     : in  std_logic; 
    rst        : in  std_logic; 
    fifo_flush : in  std_logic; 
    swap_input : in  std_logic; 
    inhibitRX  : in  std_logic; 
    reverse_rx : in  std_logic; 
    wrEn_dbg   : out std_logic; 
    din_dbg    : out std_logic_vector(9 downto 0);
    ------
    DATA1bitIN : in  std_logic := '0'; 
    elink2bit  : in  std_logic_vector (1 downto 0) := (others => '0'); -- 2 bits @ clk40, can interface 2-bit of GBT frame
    elink4bit  : in  std_logic_vector (3 downto 0) := (others => '0'); -- 4 bits @ clk40, can interface 4-bit of GBT frame
    elink8bit  : in  std_logic_vector (7 downto 0) := (others => '0'); -- 8 bits @ clk40, can interface 8-bit of GBT frame
    ------
    efifoRclk  : in  std_logic; 
    efifoRe    : in  std_logic; 
    efifoEmpty : out std_logic; 
    efifoFull  : out std_logic; 
    efifoDout  : out std_logic_vector(9 downto 0)
    ------
  ); 
end component; 
  
  component CDCC
    generic(
      NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
    port(
      clk_src    : in  std_logic; -- input clk (source clock)
      clk_dst    : in  std_logic; -- input clk (dest clock)
      data_in    : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0); -- data to be synced
      data_out_s : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)  -- synced data to clk_dst
    ); 
  end component; 
  
  -- uncomment me if input/output is serial
  attribute IOB : string;
  attribute IOB of rx_elink : signal is "TRUE";
  attribute IOB of tx_elink : signal is "TRUE";

  signal fifo_flush_i : std_logic := '1'; 
  signal rst_i        : std_logic := '1'; 
  signal fifo_flush_s : std_logic := '1'; 
  signal rst_s        : std_logic := '1'; 
  signal cnt_init     : unsigned(5 downto 0) := (others => '0');

  signal clk_des      : std_logic := '0'; 
  signal clk_ser      : std_logic := '0'; 
  signal tx_elink_i   : std_logic := '0';
  signal rx_elink_i   : std_logic := '0';
  signal tx_elink_fan : std_logic := '0';
  signal rx_elink_fan : std_logic := '0';
  
begin
  
elink_tx_inst : FIFO2Elink
  generic map(
    OutputDataRate => TX_dataRate,
    elinkEncoding  => TXRX_elinkEncoding)
  port map( 
    clk40       => clk_40,
    clk80       => clk_80,
    clk160      => clk_160,
    clk320      => clk_320,
    rst         => rst_s,
    fifo_flush  => fifo_flush_s,
    swap_output => tx_swap,
    reverse_tx  => reverse_tx,
    ------
    efifoWclk   => clk_usr,
    efifoDin    => tx_fifo_din,
    efifoWe     => tx_fifo_wr,
    efifoPfull  => tx_fifo_pfull,
    ------
    DATA1bitOUT => tx_elink_i,
    elink2bit   => tx_elink2bit,
    elink4bit   => tx_elink4bit,
    elink8bit   => tx_elink8bit
    ------
  ); 
  
elink_rx_inst : Elink2FIFO
  generic map(
    InputDataRate    => RX_dataRate,
    elinkEncoding    => TXRX_elinkEncoding,
    serialized_input => serialized_input)
  port map( 
    clk40       => clk_40,
    clk80       => clk_80,
    clk160      => clk_160,
    clk320      => clk_320,
    rst         => rst_s,
    fifo_flush  => fifo_flush_s,
    swap_input  => rx_swap,
    inhibitRX   => '0',
    reverse_rx  => reverse_rx,
    wrEn_dbg    => drdy_dbg,
    din_dbg     => din_dbg,
    ------
    DATA1bitIN  => rx_elink_i,
    elink2bit   => rx_elink2bit,
    elink4bit   => rx_elink4bit,
    elink8bit   => rx_elink8bit,
    ------
    efifoRclk   => clk_usr,
    efifoRe     => rx_fifo_rd,
    efifoEmpty  => rx_fifo_empty,
    efifoFull   => rx_fifo_full,
    efifoDout   => rx_fifo_dout
    ------
  ); 
  
-- creates an internal reset pulse for initialization/reset
initRst_proc : process(clk_usr, rst)
begin
  if(rst = '1')then
    cnt_init     <= (others => '0'); 
    rst_i        <= '1'; 
    fifo_flush_i <= '1'; 
  elsif(rising_edge(clk_usr))then
    if(cnt_init < "010000")then
      cnt_init     <= cnt_init + 1; 
      rst_i        <= '1'; 
      fifo_flush_i <= '1'; 
    elsif(cnt_init >= "010000" and cnt_init < "111111")then -- first release the flush signal
      cnt_init     <= cnt_init + 1; 
      rst_i        <= '1'; 
      fifo_flush_i <= '0'; 
    elsif(cnt_init = "111111")then -- remain in this state until reset by top
      cnt_init     <= cnt_init; 
      rst_i        <= '0'; 
      fifo_flush_i <= '0'; 
    else
      cnt_init     <= (others => '0'); 
      rst_i        <= '1'; 
      fifo_flush_i <= '1'; 
    end if; 
  end if; 
end process; 
  
CDCC_inst : CDCC
  generic map(
    NUMBER_OF_BITS => 2)
  port map(
    clk_src       => clk_usr,
    clk_dst       => clk_40,
    data_in(0)    => rst_i,
    data_in(1)    => fifo_flush_i,
    data_out_s(0) => rst_s,
    data_out_s(1) => fifo_flush_s
  ); 

serDes_generate : if (serialized_input = true) generate

-- select the ser/des clock
selClk_ila: process(clk_80, clk_160, clk_320, clk_usr)
begin
  case TX_dataRate is
    when 80     => clk_ser <= clk_80;
    when 160    => clk_ser <= clk_160;
    when 320    => clk_ser <= clk_320; 
    when others => clk_ser <= clk_usr; 
  end case;

  case RX_dataRate is
    when 80     => clk_des <= clk_80;
    when 160    => clk_des <= clk_160;
    when 320    => clk_des <= clk_320; 
    when others => clk_des <= clk_usr; 
  end case;
   
end process; 

des_proc: process(clk_des)
begin
  if(rising_edge(clk_des))then
    rx_elink_fan  <= rx_elink;

    rx_elink_i    <= rx_elink_fan;
    rx_elink_dbg  <= rx_elink_fan;
  end if;
end process;

ser_proc: process(clk_ser)
begin
  if(rising_edge(clk_ser))then
    tx_elink_fan <= tx_elink_i;

    tx_elink     <= tx_elink_fan;
    tx_elink_dbg <= tx_elink_fan;
  end if;
end process;

end generate serDes_generate;
  
end RTL; 

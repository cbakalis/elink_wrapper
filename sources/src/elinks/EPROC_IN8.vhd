----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    06/25/2014
--! Module Name:    EPROC_IN8
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee, work;
use ieee.std_logic_1164.ALL;
use work.all;

--! E-link processor, 8bit input
entity EPROC_IN8 is
generic (
            egroupID                        : integer := 0;
            EnableToHo_Egroup0Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup1Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup2Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup3Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup4Eproc8_8b10b  : boolean := true;
--IG            do_generate             : boolean := true;
            includeNoEncodingCase           : boolean := true;
    		GENERATE_FEI4B          		: boolean := false
        );
port ( 
    bitCLK      : in  std_logic;
    rst         : in  std_logic;
    ENA         : in  std_logic;
    swap_inputbits : in std_logic;
    thCR_REVERSE_10B : in std_logic;
    ENCODING    : in  std_logic_vector(1 downto 0);
    EDATA_IN    : in  std_logic_vector(7 downto 0);
    DATA_OUT    : out std_logic_vector(9 downto 0);
    DATA_RDY    : out std_logic
    );
end EPROC_IN8;

architecture Behavioral of EPROC_IN8 is

signal edata_in_s : std_logic_vector (7 downto 0);
signal data_direct_8b10b_case : std_logic_vector(9 downto 0);
signal drdy_direct_8b10b_case : std_logic;
signal ena_case0 : std_logic;
---

begin

--IG gen_enabled: if do_generate = true generate
Module_enable: if (
                        ((EnableToHo_Egroup0Eproc8_8b10b = true) and (egroupID = 0)) or
                        ((EnableToHo_Egroup1Eproc8_8b10b = true) and (egroupID = 1)) or
                        ((EnableToHo_Egroup2Eproc8_8b10b = true) and (egroupID = 2)) or
                        ((EnableToHo_Egroup3Eproc8_8b10b = true) and (egroupID = 3)) or
                        ((EnableToHo_Egroup4Eproc8_8b10b = true) and (egroupID = 4)) or
                        ((includeNoEncodingCase          = true)                   )
                    ) generate
                    
-- Reversal of the EGROUP mapping on the GBT frame is both depending on the swap input bits,
-- and the reversal of the 10b word (MSB first / LSB first).
in_sel: process(swap_inputbits,EDATA_IN, thCR_REVERSE_10B)
begin   
    if swap_inputbits = thCR_REVERSE_10B then
        edata_in_s <= EDATA_IN;
    else
        edata_in_s <= EDATA_IN(0) & EDATA_IN(1) & EDATA_IN(2) & EDATA_IN(3) & EDATA_IN(4) & EDATA_IN(5) & EDATA_IN(6) & EDATA_IN(7);
    end if;	   
end process;

-------------------------------------------------------------------------------------------
-- case 0: direct & 8b10b ENCODING b00 / b01
-------------------------------------------------------------------------------------------
ena_case0 <= '1' when (ENCODING(1) = '0' and ENA = '1') else '0';
--

direct_8b10b_case: entity work.EPROC_IN8_DEC8b10b 
generic map(
            egroupID                        => egroupID,
            EnableToHo_Egroup0Eproc8_8b10b  => EnableToHo_Egroup0Eproc8_8b10b,
            EnableToHo_Egroup1Eproc8_8b10b  => EnableToHo_Egroup1Eproc8_8b10b,
            EnableToHo_Egroup2Eproc8_8b10b  => EnableToHo_Egroup2Eproc8_8b10b,
            EnableToHo_Egroup3Eproc8_8b10b  => EnableToHo_Egroup3Eproc8_8b10b,
            EnableToHo_Egroup4Eproc8_8b10b  => EnableToHo_Egroup4Eproc8_8b10b,
            includeNoEncodingCase           => includeNoEncodingCase,
            GENERATE_FEI4B          		=> GENERATE_FEI4B
        )
port map(
    bitCLK      => bitCLK,
    rst         => rst,
    ena         => ena_case0,
	thCR_REVERSE_10B => thCR_REVERSE_10B,
    encoding    => ENCODING(0),
    edataIN     => edata_in_s,
    dataOUT     => data_direct_8b10b_case,
    dataOUTrdy  => drdy_direct_8b10b_case
	);


	
-------------------------------------------------------------------------------------------
-- case 1: HDLC ENCODING b10
-------------------------------------------------------------------------------------------
-- N/A

-------------------------------------------------------------------------------------------
-- output data/rdy according to the encoding settings
-------------------------------------------------------------------------------------------
DATA_RDY <= drdy_direct_8b10b_case;
DATA_OUT <= data_direct_8b10b_case;
--------------------
--IG end generate gen_enabled;
end generate Module_enable;
--
--IG gen_disabled: if do_generate = false generate

-- can't add the includeNoEncodingCase generic into this check since most of the time its value is in flase state
Module_almost_disable: if   (
                                ((EnableToHo_Egroup0Eproc8_8b10b = false) and (egroupID = 0)) or
                                ((EnableToHo_Egroup1Eproc8_8b10b = false) and (egroupID = 1)) or
                                ((EnableToHo_Egroup2Eproc8_8b10b = false) and (egroupID = 2)) or
                                ((EnableToHo_Egroup3Eproc8_8b10b = false) and (egroupID = 3)) or
                                ((EnableToHo_Egroup4Eproc8_8b10b = false) and (egroupID = 4))
                            ) generate

-- the module can't be disable if the generic is set
Module_disable: if  (
                        (includeNoEncodingCase = false)
                    ) generate

    DATA_OUT <= (others=>'0');
    DATA_RDY <= '0';

--IG end generate gen_disabled;
end generate Module_disable;
end generate Module_almost_disable;

end Behavioral;


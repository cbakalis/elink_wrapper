----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    18/03/2015
--! Module Name:    EPROC_OUT4
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee,work;
use ieee.std_logic_1164.all;
use work.all;

--! E-link processor, 4bit output
entity EPROC_OUT4 is
generic (
            egroupID                        : integer := 0;
            EnableFrHo_Egroup0Eproc4_8b10b  : boolean := true;
            EnableFrHo_Egroup1Eproc4_8b10b  : boolean := true;
            EnableFrHo_Egroup2Eproc4_8b10b  : boolean := true;
            EnableFrHo_Egroup3Eproc4_8b10b  : boolean := true;
            EnableFrHo_Egroup4Eproc4_8b10b  : boolean := true;
            includeNoEncodingCase           : boolean := true
        );
port ( 
    bitCLK      : in  std_logic;
    bitCLKx2    : in  std_logic;
    bitCLKx4    : in  std_logic;
    rst         : in  std_logic;
    ENA         : in  std_logic;
	swap_outbits: in std_logic;
    getDataTrig : out std_logic; -- @ bitCLKx4
    ENCODING    : in  std_logic_vector (3 downto 0);
    EDATA_OUT   : out std_logic_vector (3 downto 0);
    TTCin       : in  std_logic_vector (4 downto 0);
    fhCR_REVERSE_10B : in std_logic;
    DATA_IN     : in  std_logic_vector (9 downto 0);
    DATA_RDY    : in  std_logic
    );
end EPROC_OUT4;

architecture Behavioral of EPROC_OUT4 is

constant zeros4bit  : std_logic_vector (3 downto 0) := (others=>'0');
signal EdataOUT_ENC8b10b_case, EdataOUT_direct_case, EdataOUT_HDLC_case, EdataOUT_TTC1_case, EdataOUT_TTC2_case, EdataOUT_TTC5_case : std_logic_vector (3 downto 0);
signal rst_s, rst_case000, rst_case001, rst_case010, rst_case011, rst_case100, rst_case101 : std_logic;
signal getDataTrig_ENC8b10b_case, getDataTrig_direct_case, getDataTrig_HDLC_case, getDataTrig_TTC_cases : std_logic;
signal edata_out_s : std_logic_vector (3 downto 0);
signal ao_edata_out: std_logic_vector (3 downto 0); --IG always on data out

-- attribute mark_debug                        : string;
-- attribute mark_debug of TTCin               : signal is "true";
-- attribute mark_debug of ENCODING            : signal is "true";
-- attribute mark_debug of EdataOUT_TTC5_case  : signal is "true";
-- attribute mark_debug of edata_out_s         : signal is "true";
-- attribute mark_debug of rst                 : signal is "true";
-- attribute mark_debug of ENA                 : signal is "true";
-- attribute mark_debug of rst_case101         : signal is "true";
-- attribute mark_debug of rst_s               : signal is "true";


begin

Module_enable: if (
                        ((EnableFrHo_Egroup0Eproc4_8b10b = true) and (egroupID = 0)) or
                        ((EnableFrHo_Egroup1Eproc4_8b10b = true) and (egroupID = 1)) or
                        ((EnableFrHo_Egroup2Eproc4_8b10b = true) and (egroupID = 2)) or
                        ((EnableFrHo_Egroup3Eproc4_8b10b = true) and (egroupID = 3)) or
                        ((EnableFrHo_Egroup4Eproc4_8b10b = true) and (egroupID = 4)) or
                        ((includeNoEncodingCase          = true)                   )
                    ) generate

rst_s <= rst or (not ENA);

-------------------------------------------------------------------------------------------
-- case 0: direct data, no delimeter...
-------------------------------------------------------------------------------------------
direct_data_enabled: if includeNoEncodingCase = true generate
rst_case000 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "000")) else '1';
direct_case: entity work.EPROC_OUT4_direct 
port map(
    bitCLK      => bitCLK,
    bitCLKx2    => bitCLKx2,
    bitCLKx4    => bitCLKx4,
    rst         => rst_case000, 
    getDataTrig => getDataTrig_direct_case,    
    edataIN     => DATA_IN,
    edataINrdy  => DATA_RDY,
    EdataOUT    => EdataOUT_direct_case
    );
end generate direct_data_enabled;
--
direct_data_disabled: if includeNoEncodingCase = false generate
EdataOUT_direct_case <= (others=>'0');
end generate direct_data_disabled;
--

-------------------------------------------------------------------------------------------
-- case 1: DEC8b10b
-------------------------------------------------------------------------------------------
Enc_8b10b_generated: if (
                            ((EnableFrHo_Egroup0Eproc4_8b10b = true) and (egroupID = 0)) or
                            ((EnableFrHo_Egroup1Eproc4_8b10b = true) and (egroupID = 1)) or
                            ((EnableFrHo_Egroup2Eproc4_8b10b = true) and (egroupID = 2)) or
                            ((EnableFrHo_Egroup3Eproc4_8b10b = true) and (egroupID = 3)) or
                            ((EnableFrHo_Egroup4Eproc4_8b10b = true) and (egroupID = 4))
                        ) generate

rst_case001 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "001")) else '1';
--
ENC8b10b_case: entity work.EPROC_OUT4_ENC8b10b
port map(
    bitCLK      => bitCLK,
    bitCLKx2    => bitCLKx2,
    bitCLKx4    => bitCLKx4,
    rst         => rst_case001, 
    getDataTrig => getDataTrig_ENC8b10b_case,  
    edataIN     => DATA_IN,
    edataINrdy  => DATA_RDY,
    fhCR_REVERSE_10B => fhCR_REVERSE_10B,
    EdataOUT    => EdataOUT_ENC8b10b_case
    );

end generate Enc_8b10b_generated;

--IG: check if 8b10b decoding require
Enc_8b10b_disable: if   (
                            ((EnableFrHo_Egroup0Eproc4_8b10b = false) and (egroupID = 0)) or
                            ((EnableFrHo_Egroup1Eproc4_8b10b = false) and (egroupID = 1)) or
                            ((EnableFrHo_Egroup2Eproc4_8b10b = false) and (egroupID = 2)) or
                            ((EnableFrHo_Egroup3Eproc4_8b10b = false) and (egroupID = 3)) or
                            ((EnableFrHo_Egroup4Eproc4_8b10b = false) and (egroupID = 4))
                        ) generate

    EdataOUT_ENC8b10b_case      <= (others => '0');
    getDataTrig_ENC8b10b_case   <= '0';
    
end generate Enc_8b10b_disable;



-------------------------------------------------------------------------------------------
-- case 2: HDLC
-------------------------------------------------------------------------------------------
rst_case010 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "010")) else '1';
--
getDataTrig_HDLC_case   <= '0'; --'1' when (ENCODING(2 downto 0) = "010") else '0';
EdataOUT_HDLC_case      <= (others=>'0'); --<---TBD
--


-------------------------------------------------------------------------------------------
-- output data and busy according to the encoding settings
-------------------------------------------------------------------------------------------
process(EdataOUT_direct_case,EdataOUT_ENC8b10b_case,EdataOUT_HDLC_case,ENCODING)
begin
    case ENCODING(1 downto 0) is 
        when "00"   => edata_out_s <= EdataOUT_direct_case;
        when "01"   => edata_out_s <= EdataOUT_ENC8b10b_case;
        when "10"   => edata_out_s <= EdataOUT_HDLC_case;
        when others => edata_out_s <= (others => '0');
    end case;
end process;

--
getDataTrig  <= ENA and (getDataTrig_HDLC_case or getDataTrig_ENC8b10b_case or getDataTrig_direct_case);
--

end generate Module_enable;

--

-- can't add the includeNoEncodingCase generic into this check since most of the time its value is flase state
Module_almost_disable: if   (
                                ((EnableFrHo_Egroup0Eproc4_8b10b = false) and (egroupID = 0)) or
                                ((EnableFrHo_Egroup1Eproc4_8b10b = false) and (egroupID = 1)) or
                                ((EnableFrHo_Egroup2Eproc4_8b10b = false) and (egroupID = 2)) or
                                ((EnableFrHo_Egroup3Eproc4_8b10b = false) and (egroupID = 3)) or
                                ((EnableFrHo_Egroup4Eproc4_8b10b = false) and (egroupID = 4))
                            ) generate

-- the module can't be disable if the generic is set
Module_disable: if  (
                        (includeNoEncodingCase = false)
                    ) generate

    edata_out_s <= (others=>'0');
    getDataTrig <= '0';

end generate Module_disable;
end generate Module_almost_disable;


--******************
--* ALWAYS ON ZONE *
--******************

-------------------------------------------------------------------------------------------
-- case 3: TTC-1  
-------------------------------------------------------------------------------------------
rst_case011 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "011")) else '1';
--
ttc_r3: process(bitCLK)
begin
    if bitCLK'event and bitCLK = '1' then
        if (rst_case011 = '1') then
            EdataOUT_TTC1_case <= zeros4bit;
        else
            EdataOUT_TTC1_case <= TTCin(1) & TTCin(3 downto 2) & TTCin(0);
        end if;	   
	end if;
end process;
--
-------------------------------------------------------------------------------------------
-- case 4: TTC-2  
-------------------------------------------------------------------------------------------
rst_case100 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "100")) else '1';
--
ttc_r4: process(bitCLK)
begin
    if bitCLK'event and bitCLK = '1' then
        if (rst_case100 = '1') then
            EdataOUT_TTC2_case <= zeros4bit;
        else
            EdataOUT_TTC2_case <= TTCin(4 downto 2) & TTCin(0);
        end if;	   
	end if;
end process;
--
-------------------------------------------------------------------------------------------
-- case 5: TTC-5  
-------------------------------------------------------------------------------------------
rst_case101 <= '0' when ((rst_s = '0') and (ENCODING(2 downto 0) = "101")) else '1';
--
ttc_r5: process(bitCLK)
begin
    if bitCLK'event and bitCLK = '1' then
        if (rst_case101 = '1') then
            EdataOUT_TTC5_case <= zeros4bit;
        else
            EdataOUT_TTC5_case <= (TTCin(2) & TTCin(2) & TTCin(2) & TTCin(2)); -- expend BCR signal over the entire 25nSec (4x BCR bit)
        end if;	   
	end if;
end process;
--

-------------------------------------------------------------------------------------------
-- output data and busy according to the encoding settings
-------------------------------------------------------------------------------------------
dataOUTmux: entity work.MUX8_Nbit 
generic map (N=>4)
port map( 
	data0    => edata_out_s,
	data1    => edata_out_s,
	data2    => edata_out_s,
	data3    => EdataOUT_TTC1_case,
	data4    => EdataOUT_TTC2_case,
	data5    => EdataOUT_TTC5_case,
	data6    => zeros4bit,
	data7    => zeros4bit,
	sel      => ENCODING(2 downto 0),
	data_out => ao_edata_out
	);
--

out_sel: process(swap_outbits,ao_edata_out)
begin   
    if swap_outbits = '1' then
        EDATA_OUT <= ao_edata_out(0) & ao_edata_out(1) & ao_edata_out(2) & ao_edata_out(3);
    else
        EDATA_OUT <= ao_edata_out;
    end if;	   
end process;


end Behavioral;


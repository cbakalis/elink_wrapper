----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: Israel Grayzman (israel.graymzna@weizmann.ac.il)
--! 
--! Create Date:    01/19/2019 
--! Module Name:    EPROC_IN2_HDLC
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee, work;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL; 
use work.centralRouter_package.all;
use work.all;

--! HDLC decoder for EPROC_IN2 module
entity EPROC_IN2_HDLC is
port (  
    bitCLK      : in  std_logic;
    bitCLKx2    : in  std_logic;
    rst         : in  std_logic;
    ena         : in  std_logic;
    edataIN     : in  std_logic_vector (1 downto 0);
    dataOUT     : out std_logic_vector(9 downto 0);
    dataOUTrdy  : out std_logic
    );
end EPROC_IN2_HDLC;

architecture Behavioral of EPROC_IN2_HDLC is

signal input_register               : std_logic_vector(9 downto 0);
signal isflag_H                     : std_logic;
signal isflag_L                     : std_logic;
signal iserror                      : std_logic;
signal remove_zero_H                : std_logic;
signal remove_zero_L                : std_logic;
signal error_state                  : std_logic;
signal receiving_state              : std_logic; -- set sign message decoding
signal start_of_a_message           : std_logic;
signal output_register_location     : integer range 0 to  3; -- 2 bits implementation
signal output_register              : std_logic_vector(7 downto 0);
signal output_register_ready        : std_logic;
signal store_the_first_byte         : std_logic_vector(7 downto 0); -- during back2back messages, the first data byte of the second message must be store in order to generate a SOC header
signal write_the_first_byte         : std_logic; -- write the first stored byte in the second message during back2back messages
signal low_bit_position             : integer range 0 to 15; -- 4 bits implementation, even though only values 6 and 7 are valid
signal high_bit_position            : integer range 0 to 15; -- 4 bits implementation, even though only values 7 and 8 are valid
signal hold_i                       : std_logic; -- hold immidiately
signal hold_d                       : std_logic; -- hold with one clock delay
signal hold                         : std_logic; -- set to hold the writing to output_register
signal shift                        : std_logic; -- set when need to remove '0' bit stuffing

-- for testing purposes only through simulation and/or chipscope
signal SelectedBits                 : std_logic_vector(1 downto 0);
signal message_status_4_debug   	: std_logic_vector(1 downto 0);
signal message_ready_4_debug    	: std_logic;

--IG attribute mark_debug                            	: string;
--IG --attribute mark_debug of ena                     	: signal is "true";
--IG attribute mark_debug of input_register          	: signal is "true";
--IG attribute mark_debug of output_register         	: signal is "true";
--IG attribute mark_debug of output_register_ready   	: signal is "true";
--IG attribute mark_debug of store_the_first_byte    	: signal is "true";
--IG attribute mark_debug of write_the_first_byte    	: signal is "true";
--IG attribute mark_debug of isflag_H                	: signal is "true";
--IG attribute mark_debug of isflag_L                	: signal is "true";
--IG attribute mark_debug of remove_zero_H           	: signal is "true";
--IG attribute mark_debug of remove_zero_L           	: signal is "true";
--IG attribute mark_debug of iserror                 	: signal is "true";
--IG attribute mark_debug of error_state             	: signal is "true";
--IG attribute mark_debug of receiving_state         	: signal is "true";
--IG attribute mark_debug of start_of_a_message      	: signal is "true";
--IG attribute mark_debug of message_status_4_debug  	: signal is "true";
--IG attribute mark_debug of message_ready_4_debug   	: signal is "true";
--IG attribute mark_debug of output_register_location	: signal is "true";
--IG attribute mark_debug of low_bit_position        	: signal is "true";
--IG attribute mark_debug of high_bit_position       	: signal is "true";
--IG attribute mark_debug of hold_i                  	: signal is "true";
--IG attribute mark_debug of hold_d                  	: signal is "true";
--IG attribute mark_debug of hold                    	: signal is "true";
--IG attribute mark_debug of shift                   	: signal is "true";
--IG attribute mark_debug of SelectedBits            	: signal is "true";

begin

-------------------------------------------------------------------------------------------
--live bitstream
-- input serializer
-------------------------------------------------------------------------------------------
process(rst, bitCLK)
begin
    if (rst = '1') then
        input_register    <= (others => '0');
    elsif rising_edge(bitCLK) then
        input_register    <= edataIN(0) & edataIN(1) & input_register(9 downto 2); -- HDLC bits are swapped by default
    end if;
end process;

    -- High (_H) or Low (_L) defines the location of the flag
    isflag_H        <=  '1' when  (input_register(9 downto 2) = "01111110") else '0';        
    isflag_L        <=  '1' when  (input_register(8 downto 1) = "01111110") else '0';        
    
    iserror         <=  '1' when ((input_register(8 downto 2) = "1111111" ) or (input_register(9 downto 3) = "1111111")) else '0';
    
    -- High (_H) or Low (_L) defines the location of the '0' bit stuffing
    remove_zero_H   <=  '1' when  (input_register(9 downto 4) = "011111"  ) else '0';
    remove_zero_L   <=  '1' when  (input_register(8 downto 3) = "011111"  ) else '0';

process(rst, bitCLK)
begin
    if (rst = '1') then
        error_state                 <= '1';
        receiving_state             <= '0';
        start_of_a_message          <= '0';
    elsif rising_edge(bitCLK) then
        if (iserror = '1') then
            error_state     <= '1';
        elsif ((isflag_H = '1') or (isflag_L = '1')) then
            error_state     <= '0';
        else
            error_state     <= error_state;
        end if;

        -- error resets the values
        if (iserror = '1') then
            receiving_state  <= '0';
        -- set when detecting an HDLC flag (support back2back messages)
        elsif ((isflag_H = '1') or (isflag_L = '1')) then
            receiving_state  <= '1';
        else
            receiving_state  <= receiving_state;
        end if;
        
        -- set to identify the beggining of a second message (back2back)
        if ((receiving_state = '1') and ((isflag_H = '1') or (isflag_L = '1'))) then
            start_of_a_message          <= '1';
        -- clear with output_register_ready means a back2back message, clear with iserror means regular message
        elsif ((output_register_ready = '1') or (iserror = '1')) then
            start_of_a_message          <= '0';
        else
            start_of_a_message          <= start_of_a_message;
        end if;
    end if;
end process;

-- removing '0' bit stuffing mechnism.
-- the output register should get the data from the input register without the '0' bit stuff of the encoder.
-- when a remove_zero_L is set, meaning the '0' bit stuffing locate in bit #8 of the input register.
-- when a remove_zero_H is set, meaning the '0' bit stuffing locate in bit #9 of the input register.
-- shift signal set when removing the '0' bit stuffing changed the orientation of the data received from the input register.
-- hold signal set when need to wait one clock to clean data that already read from the input register in the default position.
--
-- * remove_zero_L or remove_zero_H should set together with isflag_L or isflag_H,
--   Due to the priority mux, the isflag_L or isflag_H are handle instead of remove_zero_L or remove_zero_H. 
process(rst, bitCLK)
begin
    if (rst = '1') then
        shift               <= '0';
        hold_d              <= '0';
        low_bit_position    <=   6; -- default non shift position
        high_bit_position   <=   7; -- default non shift position
    elsif rising_edge(bitCLK) then
        -- error resets the values
        if (((isflag_L = '0') and (isflag_H = '0')) and ((iserror = '1') or (error_state = '1'))) then
            shift               <= '0';
            hold_d              <= '0'; --hold delay
            low_bit_position    <=   6; -- default non shift position
            high_bit_position   <=   7; -- default non shift position
        elsif (shift = '0') then
            if    (isflag_L = '1') then
                shift               <= '1';
                hold_d              <= '0';
                low_bit_position    <=   7;
                high_bit_position   <=   8;
            elsif (isflag_H = '1') then
                shift               <= '0';
                hold_d              <= '1'; -- hold one clock to clean position 7:6 from the flag value
                low_bit_position    <=   6; -- default non shift position
                high_bit_position   <=   7; -- default non shift position
            elsif (remove_zero_L = '1') then
                shift               <= '1';
                hold_d              <= '0';
                low_bit_position    <=   7; -- default shift position
                high_bit_position   <=   8; -- default shift position
            elsif (remove_zero_H = '1') then
                shift               <= '1';
                hold_d              <= '0';
                low_bit_position    <=   6;
                high_bit_position   <=   8;
            -- default values
            else
                shift               <= '0';
                hold_d              <= '0'; --shift delay
                low_bit_position    <=   6; -- default non shift position
                high_bit_position   <=   7; -- default non shift position
            end if;
        else -- (shift = '1')
            if    (isflag_L = '1') then
                shift               <= '1';
                hold_d              <= '0'; -- keep collecting the data immediately
                low_bit_position    <=  7;
                high_bit_position   <=  8;
            elsif (isflag_H = '1') then
                shift               <= '0';
                hold_d              <= '1';
                low_bit_position    <=   6;
                high_bit_position   <=   7;
            elsif (remove_zero_L = '1') then
                shift               <= '0';
                hold_d              <= '0';
                low_bit_position    <=  5;
                high_bit_position   <=  7;
            elsif (remove_zero_H = '1') then
                shift               <= '0';
                hold_d              <= '1';
                low_bit_position    <=   6;
                high_bit_position   <=   7;
            -- default values
            else
                shift               <= '1';
                hold_d              <= '0';
                low_bit_position    <=   7;-- default shift position
                high_bit_position   <=   8;-- default shift position
            end if;
        end if;
    end if;
end process;

hold_i  <=  '1'  when ((remove_zero_L = '1') and (shift = '1')) else
            '0';
-- set the hold value
hold    <=  hold_d or hold_i;

-- defines the write location of the bits from the input_register to the output_register
process(rst, bitCLK)
begin
    if (rst = '1') then
        output_register_location    <=   0;
        output_register_ready       <= '0';
    elsif rising_edge(bitCLK) then
        -- error resets the values
        if (iserror = '1') then
            output_register_location    <=   0;
            output_register_ready       <= '0';
        elsif (receiving_state = '1') then
            -- back2back messages, handaling the second message
            -- isflag_X signal set, therefore output_register_location should be clear
            -- to start collecting the bits of the second message if arrive.
            if ((isflag_L = '1') or (isflag_H = '1')) then
                output_register_location    <=   0;
                output_register_ready       <= '0';
            -- stalling the output_register_location counter while hold is set (during transitions)
            -- OR while ((remove_zero_L = '1') and (shift = '1')) since remove_zero_L required immidiate action
            elsif (hold = '1') then
                output_register_location    <= output_register_location;
                output_register_ready       <= '0';
            else
                -- new data is ready, 8 bits collected in the output_register register
                if (output_register_location = 3) then
                    output_register_location    <=   0;
                    output_register_ready       <= '1';
                else
                    output_register_location    <= output_register_location + 1;
                    output_register_ready       <= '0';
                end if;
            end if;
        else
            output_register_location    <=   0;
            output_register_ready       <= '0';
        end if;
    end if;
end process;

-- output register
-- store two bits at a time 
process(rst, bitCLK)
begin
    if (rst = '1') then
        output_register   <= (others => '0');
    elsif rising_edge(bitCLK) then
        -- store the decode data in position 0 of the output register
        if    (output_register_location = 0) then
            output_register(0)        <= input_register(low_bit_position);
            output_register(1)        <= input_register(high_bit_position);
        -- store the decode data in position 1 of the output register
        elsif (output_register_location = 1) then
            output_register(2)        <= input_register(low_bit_position);
            output_register(3)        <= input_register(high_bit_position);
        -- store the decode data in position 2 of the output register
        elsif (output_register_location = 2) then
            output_register(4)        <= input_register(low_bit_position);
            output_register(5)        <= input_register(high_bit_position);
        -- store the decode data in position 3 of the output register
        else--(output_register_location = 3)
            output_register(6)        <= input_register(low_bit_position);
            output_register(7)        <= input_register(high_bit_position);
        end if;
    end if;
end process;
-- for testing purposes only through simulation and/or chipscope
SelectedBits(0)   <= input_register(low_bit_position);
SelectedBits(1)   <= input_register(high_bit_position);

-- output data
-- determine the value of data out ready and data out code
process(rst, bitCLK)
begin
    if (rst = '1') then
        store_the_first_byte    <= (others => '0');
        write_the_first_byte    <= '0';
        dataOUT                 <= (others => '0');
        dataOUTrdy              <= '0';
        message_status_4_debug  <= "00";
        message_ready_4_debug   <= '0';
    elsif rising_edge(bitCLK) then
        -- HDLC enabled
        if (ena = '1') then
            -- error state is the HDLC IDLE state
            if (iserror = '1') then
                write_the_first_byte    <= '0';
                dataOUT(9 downto 8)     <= "11";
                dataOUTrdy              <= '1';
                message_status_4_debug  <= "11";
                message_ready_4_debug   <= '1';
            -- waiting for a new message
            elsif (receiving_state = '0') then
                write_the_first_byte    <= '0';
                -- SOC (the data content doesn't matter)
                dataOUT(9 downto 8)     <= "10";
                message_status_4_debug  <= "10";
                -- writing SOC 
                if ((isflag_H = '1') or (isflag_L = '1')) then
                    dataOUTrdy              <= '1';
                    message_ready_4_debug   <= '1';
                else
                    dataOUTrdy              <= '0';
                    message_ready_4_debug   <= '0';
                end if;
            -- waiting to the end of the message
            else -- (receiving_state = '1')
                -- EOC (the data content doesn't matter) 
                if    ((isflag_H = '1') or (isflag_L = '1')) then
                    write_the_first_byte    <= '0';
                    dataOUT(9 downto 8)     <= "01";
                    dataOUTrdy              <= '1';
                    message_status_4_debug  <= "01";
                    message_ready_4_debug   <= '1';
                -- writing out-of-band SOC - back2back message 
                elsif ((start_of_a_message = '1') and (output_register_ready = '1')) then
                    write_the_first_byte    <= '1'; -- set to send again the first byte (since the received one used to generate a SOC)
                    -- SOC (the data content doesn't matter)
                    dataOUT(9 downto 8)     <= "10";
                    dataOUTrdy              <= '1';
                    message_status_4_debug  <= "10";
                    message_ready_4_debug   <= '1';
                -- data
                elsif ((output_register_ready = '1') or (write_the_first_byte = '1')) then 
                    write_the_first_byte    <= '0';
                    dataOUT(9 downto 8)     <= "00";
                    dataOUTrdy              <= '1';
                    message_status_4_debug  <= "00";
                    message_ready_4_debug   <= '1';
                else
                    write_the_first_byte    <= '0';
                    dataOUT(9 downto 8)     <= "00";
                    dataOUTrdy              <= '0';
                    message_status_4_debug  <= "00";
                    message_ready_4_debug   <= '0';
                end if;
            end if;
        -- HDLC disabled
        else -- (ena = '0')
            write_the_first_byte    <= '0';
            dataOUT(9 downto 8)     <= "00";
            dataOUTrdy              <= '0';
            message_status_4_debug  <= "00";
            message_ready_4_debug   <= '0';
        end if;

        store_the_first_byte    <= output_register;
        -- data out
        if (write_the_first_byte = '0') then
            dataOUT(7 downto 0)     <= output_register;
        -- during the second message of a back2back messages
        else
            dataOUT(7 downto 0)     <= store_the_first_byte;
        end if;
    end if;
end process;

end Behavioral;

